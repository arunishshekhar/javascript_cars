// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and 
//log the results in the console as it was returned.

function sortAlphabatically(inventory)
{
    /*let models=[]
    for (let index = 0; index < inventory.length; index++) 
    {
        models.push(inventory[index]['car_model']);
    }

    models.sort()

    for (let index = 0; index < models.length; index++) 
    {
        for (let i = 0; i < inventory.length; i++) 
        {
            if (models[index] === inventory[i]['car_model'])
            {
                console.log(inventory[i]);
                inventory.splice(i,1)
                //console.log(inventory.length)
                break;
            }
            
        }
        
    }*///Takes 8 milliseconds
    //inventory.sort((a,b) => (a['car_model'] > b['car_model']) ? 1 : -1) //takes 6 milliseconds
    inventory.sort(function(a, b) 
    {
        var nameA = a['car_model'].toUpperCase(); // ignore upper and lowercase
        var nameB = b['car_model'].toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
    });
    return inventory;
}

module.exports = sortAlphabatically;

