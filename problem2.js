// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory.
// Execute a function to find what the make and model of the last car in the 
//inventory is?  Log the make and model into the console in the format of: 
//"Last car is a *car make goes here* *car model goes here*"
const inventory = require('./cars.js');
function latestModel(inventory)
{
    let latest = inventory[0]["car_year"];
    for (let index = 0; index < inventory.length; index++) 
    {
        if (inventory[index]["car_year"] > latest)
        {
            latest = inventory[index]["car_year"];
        }
        
    }
    for (let index = 0; index < inventory.length; index++) 
    {
        if (inventory[index]["car_year"] === latest)
        {
            console.log(`Last car is a ${inventory[index]["car_make"]} ${inventory[index]["car_model"]}`);
        }
    }

}

function lastCar(inventory)
{
    //let val = inventory.length
    //console.log(val);
    console.log(`Last car is a ${inventory[inventory.length-1]["car_make"]} ${inventory[inventory.length-1]["car_model"]}`);
}

lastCar(inventory)
module.exports = latestModel;