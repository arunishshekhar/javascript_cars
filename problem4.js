// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. 
//Execute a function that will return an array from the dealer data containing 
//only the car years and log the result in the console as it was returned.

function yearOnly(inventory)
{
    let models=[]
    for (let index = 0; index < inventory.length; index++) 
    {
        models.push(inventory[index]['car_year']);
    }
    return models;
}

module.exports = yearOnly;